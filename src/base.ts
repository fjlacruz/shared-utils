import express from 'express';
import serverless from 'serverless-http';
import servicesRouter from './routes';

const app = express();
app.use('/services', servicesRouter);

module.exports.generic = serverless(app);

import { Router, Request, Response } from 'express';

const router = Router();

router.get('/', (req: Request, res: Response) => {
  const host = process.env.DB_HOST;

  res.json({
    message: 'Lambda CICD',
    host,
  });
});

router.get('/service1', (req: Request, res: Response) => {
  res.json({ message: 'This is Service 1' });
});

router.get('/service2', (req: Request, res: Response) => {
  res.json({ message: 'This is Service 2' });
});

export default router;

import request from 'supertest';
import express from 'express';
//import { mocked } from 'ts-jest/utils'; // Si se necesita para el mock

// Importa el archivo de rutas
import servicesRouter from '../routes'; // Ajusta la ruta según tu estructura

// Crea una aplicación Express y monta las rutas
const app = express();
app.use('/services', servicesRouter);

describe('Services Route Tests', () => {
  it('should respond with status 200 for /services', async () => {
    const response = await request(app).get('/services');
    expect(response.status).toBe(200);
  });

  it('should respond with correct message for /services/service1', async () => {
    const response = await request(app).get('/services/service1');
    expect(response.body.message).toBe('This is Service 1');
  });

  it('should respond with correct message for /services/service2', async () => {
    const response = await request(app).get('/services/service2');
    expect(response.body.message).toBe('This is Service 2');
  });

  // Agrega más pruebas según sea necesario
});
